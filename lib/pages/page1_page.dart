import 'package:flutter/material.dart';
import 'package:transicion_app/pages/page2_page.dart';

class PagePage1 extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Page 1'),
      ),
      body: Center(
        child: Text(
          'Working Page 1'
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(context, _createRoot());
        },
        child: Icon(Icons.access_time_sharp),
      ),
    );
  }

  Route _createRoot() {
    return PageRouteBuilder(pageBuilder: (BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation) => Page2Page(),
      transitionDuration:  const Duration(seconds: 1),
      transitionsBuilder: (context, animation, secondaryAnimation, child){
        final curveAnimation = CurvedAnimation(parent: animation, curve: Curves.easeInOut);
        /*return SlideTransition(position: Tween<Offset>(begin: Offset(0.0, 1.0), end: Offset.zero).animate(curveAnimation),
        child: child,
        );*/

        /*return ScaleTransition(
            scale: Tween<double>(begin: 0.0, end: 1.0).animate(curveAnimation),
            child: child);*/

        /*return RotationTransition(turns: Tween<double>(begin: 0.0, end: 1.0).animate(curveAnimation), child: child);*/

        return FadeTransition(opacity: Tween<double>(begin: 0.0, end: 1.0).animate(curveAnimation), child: child);
      }
    );
  }
}
